import socket, httplib, urllib

# Invoke this script using fuzzer.py <RHOST> <RPORT>

# Configure remote IP/Port variables
RHOST = sys.argv[1]
RPORT = sys.argv[2]

# Define your buffer string that will be incremented until a potential crash
buffer = '\x41' * 50

# Create a loop that will connect to the service and send the buffer
while True:
	try:
		# Send buffer, increment by 50
		buffer = buffer + '\x41' * 50
	except:
		print "Buffer Length: " + len(buffer)
		print "Cannot connect to service...check debugger for a potential crash"